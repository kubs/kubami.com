---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
summary:
url: ""
linkToMarkdown: true
toc:
  enable: false
---
