---
title: Consultation sessions with Kuba
date: 2022-01-02
summary: Get advice from Kuba Misiorny
url: "/consultation-sessions-with-kuba"
linkToMarkdown: true
toc:
  enable: false
featuredImage:
  - name: "featured-image"
    src: "images/climb.jpg"

lightgallery: true
---

Hi there,

I love to help people and see them succeed. Most of my project are aimed at that. Maybe your need is answered in one of my projects.
If you, however think my direct input would be valuable to your work you can book a video call session with me.

Some things I helped people and companies with:
- Business idea validation, product deconstruction,
- Landing page review,
- Product (UI/UX/user flows) feedback and review,
- Technical roadmap review, coding help
- Help setting up anything technical in your startup (hosting, servers, domains, DNS, etc.)

You can book a session below.

:hourglass: 30-minute session
{{< style "font-size:30px; margin-bottom: 10px" >}}
£450
{{< /style >}}
[*Book Now* :green_book:](https://kubami.gumroad.com/l/consultation-30)

<div><br></div>

:alarm_clock: 60-minut session
{{< style "font-size:30px" >}}
£850
{{< /style >}}
[*Book Now* :blue_book:](https://kubami.gumroad.com/l/consultation-60)

---
## Terms and Conditions
- I agree to participate in an online video call session with you.
- I will help you with anything that is within my expertise and that hopefully will be valuable for you.
- I will refund you in full if our session cannot be arranged, or there any other technical difficulties from my side.
- The fee is agreed upon before the session and is paid in advance.
- I will refund you 50% of the fee if you are not happy with the session within the first 15 minutes.
- I will treat our conversation as confidential, however I will not sign an NDA.
- You can record the session, for your personal use. However, please consult with me before using it in any other way, i.e., posting it publicly.
- I will not be held responsible for any damages/liabilities/losses that may result from my suggestions, feedback, or from anything else we discuss.
- If any Intellectual property is created during our conversation we shall resolve the ownership of that in good faith.
- These Terms of Service shall be governed by and interpreted in accordance with the laws of United Kingdom.
