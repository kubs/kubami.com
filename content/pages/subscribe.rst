Keep in touch
################

:slug: subscribe

.. raw:: html

	<form action="https://tinyletter.com/kubami" method="post" target="popupwindow" 
		onsubmit="window.open('https://tinyletter.com/kubami', 'popupwindow', 'scrollbars=yes,width=800,height=600');return true"
		class="m-block m-default m-success"
		>
	<p><label for="tlemail">Fantastic articles and updates from Kuba await you. Just drop in your email address.</label></p>
	<p><input type="text" style="width:140px;border:1px white solid;line-height: 30px;padding-left: 5px;" name="email" id="tlemail" /></p>
	<input type="hidden" value="1" name="embed"/>
	
	<div class="m-button m-primary">
	 <a href="#" onclick="this.parentNode.parentNode.submit();">
	    <div class="m-big">Let's go</div>
	    <div class="m-small">Subscribe</div>
	  </a>
	</div>
	<p class="m-text m-small m-dim">Subscribe button will redirect to TinyLetter</a></p></form>
        