---
title: About Kuba's Lab
date: 2021-03-09
summary: About Kuba Misiorny's lab.
url: "/about"
linkToMarkdown: true
toc:
  enable: false
---

By day (and sometimes night) I am a CTO at [untrite.com](https://untrite.com>), the rest of the time... well, at least part of that journey is here.

I always wanted to have a lab, a cave, a garage...
Maybe that thought comes from watching Dexter's Laboratory when I was little, or a little too much Q-branch from James Bond.
But what I want to believe is that I just love experimentation, figuring things out and then combining all that hard-earned knowledge to create something new.

At least *new to me*.

So here it is, my journey.
