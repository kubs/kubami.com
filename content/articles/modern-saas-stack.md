---
title: My modern SaaS software stack
date: 2021-03-09
summary: Best stack for building SaaS is the one you know the best.
---

> The best stack for building SaaS is the one you know the best.

The above quotes is true when you are just beginning. What counts then is *shipping it*.
Build it, ship it and spend the majority of your time talking to your (potential) users
(Insert mandatory praise for [The Mom Test](http://momtestbook.com/) here.

I want to focus on completeness and production-readiness. It's great that your prototype
is working on your local machine, but how are you going to serve it to your potential users?

What is the glue that connects all of the parts?

# General

- Python 3 - https://www.python.org/ (currently >3.8)
    We do a lot of Machine Learning and Data Science, python is a de-facto standard in this area. That gives us access to *the shoulders of giants* - and I mean both state-of-the-art in ML research and largest talent pool.
- *Poetry* - https://python-poetry.org
    I am a big believer in the `pyproject.toml`. Poetry is *the* tool for this new format.
- Typescript - https://www.typescriptlang.org/
    React on the frontend means Javascript, and really, Javascript should mean Typescript.
- *Terraform* - https://www.terraform.io/
    Infrastructure as code for the win!
    We need many instances of the whole infrastructure for key clients. Doing this work by hand with a checklist would be a terrible waste of time and room for error.
    I love that our infrastructure is versioned, and I can at a glance introspect it in a text file, or with a CLI.

- *Cookiecutter* - https://cookiecutter.readthedocs.io
    Especially at the beginning of my journey, I was stamping out PoCs and demos day and night. Django scaffold project and `create-react-app` are great but very basic. I have developed a lot of glue code to get the foundational functionality. Things like, graphql, JWT authentication, celery, logging, default UI layouts, login dialogs, etc.
    I am working on publish my full stack cookiecutter template.

- *GraphQL* - https://graphql.org
    Lots of debate about this vs traditional (and battle-tested) REST APIs. It is true, once you get deep enough and want to do more subtle things it turns out you need to write lots of glue code, and try to salvage half-dead open source projects. At time like this I wish I made better decisions.
    For the most part, working with Apollo makes up for this.

# Backend

- *Django 3* - https://www.djangoproject.com/
- *PostgreSQL*
    The DB to use with Django.
- *Celery* - https://docs.celeryproject.org - distributed task queue
    User for running distributed jobs. We use it both for batch jobs, like retraining ML models, but also to fan-out paralleralizable on-line jobs. There is a million ways to do that more efficiently and smarter - don't need that optimisation yet
- *Redis* - https://redis.io/
    - as a queue for Celery
    - as cache for Django
- *Docker* - https://docs.docker.com/
    Yes, it might add overhead - both performance wise, and complexity wise - but at the end of the day, you deal with the complexity once and it works on everybody's system the same.
- *Sentry* - https://sentry.io - to catch production bugs both on the frontend and backend
- *OpenSearch + OpenSearchDashboard (Kibana) + Filebeat* - https://opensearch.org/ - logging and analytics
    Q4 2021 Update: We now use OpenSearch in production.
    We used to:
    > We use OSS versions of this software. After the recent sheneningans from elastic.co, we're planning on moving to opendistro.
    > We use elasticsearch extensively in our business logic, so it's only sane to reuse that infrastructure for logging.
- *Caddy* - https://caddyserver.com -
    Used as a reverse proxy with HTTPS and static file server
    Initially I have used nginx, but now moved on to Caddy. Configuration is a breeze and you get "free" HTTPs from Letsencrypt.

# Frontend

- *React* - https://reactjs.org/
    This is where I chose this technology because I know it. Another very important point is the community, wealth of third-party packages and the talent pool.
- *Apollo GraphQL* - https://www.apollographql.com - GraphQL communication layer for frontend
- *Yarn* - https://yarnpkg.com/ - who still uses npm?
- *blueprint.js* - https://blueprintjs.com - React component library
    I love Blueprint.js! Guys at Palantir did a fantastic job. Fair warning, components in blueprint are geared towards data heavy, professional applications. It wouldn't be my first choice for shiny consumer app.
    There is no official "mobile support". But the library is so well written, along with simple `flexbox` scaffolding it's really easy to bake in basic, perfectly usable responsiveness all the way to mobile support. For serious applications you'd want to have a separate mobile version anyway.
- *GraphQL Codegen* - [graphql-code-generator.com](graphql-code-generator.com>) - go from graphql requests to fully fleshed out react hooks
    Indispensable tool for working with GraphQL, Typescript and IDE.

# Tools

- *JetBrains IDEs*
    I grew up on [VIM](https://www.vim.org), but got tired of configuring autocompletion, I made the switch and never looked back - that is after porting some of my most used keybindings.
    I use PyCharm for the backend, Django development and Webstorm for the frontend.

- *GitLab* - source control, dev wikis, dev documentation
    I love gitlab. I am not sure why it is not more popular.
    Initially I have chose it because they offered free private repos.
- *Black* - `The uncompromising Python code formatter`
    Just use it.
    Blackened code looks the same regardless of the project you're reading. Formatting becomes transparent after a while and you can focus on the content instead.
- *Prettier* - Code formatter for Javascript/Typescript
