---
title: The devil is in the (too much) details
date: 2022-09-03
summary: "How to be a better technical communicator? Spoiler: be less technical."
---

A big part of my job is being a translator. Translator between worlds. A world of business and a world of technology.

# Walking down the tree
This, of course, doesn’t mean I take words or even phrases and try to find their "translation".

As any good translator I take the _concepts_, _the context, and most importantly _who my audience_ is into consideration. I then craft stories that, given my audiences' background, will resonate with them. I think one of the keys to successfully doing this is having a map of the common pitfalls.

- Where are the gaps of knowledge I might pre-empt?
- What do they know I can leverage?

Even now, after years of practice, I catch myself mid-sentence when I need to "stop and walk up the tree"[1]. This happens when I realise, the person I am talking to might not know the concept that would be crucial to understanding what I am about to say.
You can imagine this as exploring the tree of knowledge with the person until you hit the nodes they already know. You can then start building up till you explained the concept.

This works when you are talking to your peers in a certain subject. I will do that when talking to engineers in my team. Through this, we can build shared understanding of deep technical topics. 

The problem begins when you try to explain topics to someone "from a different world". 
But actually, do they need to understand **this**, or **that**? 

There is another way of conveying the point.


# Do they need to understand "this"? 
{{< fig src="images/stargate_wormholes.jpg" caption="Bonus point if you get the MacGyver reference." >}}

It's very crucial to learn how to describe things on different levels of complexity. 

Most importantly, to learn to modulate the level of technical detail at which you describe things and establish trust with your intended audience that your level of technical detail matches the nature of the issue. This gives your audience a clue that when you throw around jargon, it's because it is important to understand, and they need to care about it.

This is a similar problem map-makers face. The only way to faithfully represent an area, would be to have a "map" that is the size of that are, replicating everything in it. That wouldn't be a very useful map. So map-makers devised special symbols and ways of getting rid of unecessary details, to make maps readable.
Going even further with that analogy, there are different maps for different purposes, underground (metro) maps lack all details of overground lay of the land, precisely not to confuse the reader. 
{{< fig src="images/london_tube.jpg" caption="Can you navigate easily the underground lines?" >}}

Imagine trying to navigate London Tube using a road map. All the details are actively adding noise, making the point less apparent. Don't be like that when explaining technical topics. 

There is, of course, an XKCD comic illustrating this point.

![How could anyone consider themselves a well-rounded adult without a basic understanding of silicate geochemistry? Silicates are everywhere! It's hard to throw a rock without throwing one!](https://imgs.xkcd.com/comics/average_familiarity.jpg "Logo Title Text 1")





# Why should I be less technical? Isn't it better if other people learn the technical language?
{{< fig src="images/stargate_1.jpg" caption="Bonus point if you get the MacGyver reference." >}}

This paragraph shouldn't even be here. My view is this is my job. And... I enjoy it. This is what teachers are talking about - the exhiliration when you see the twinkle of understanding in someone's eyes.
Another point is that, you already have this ability to translate. You are already doing it in your head, consciously or not. And here is the crux, most of the deeply technical people have been thinking in tech-speak so long they forgot how they know things. 

What helps me is imagining what the other person already knows and using that as a bedrock. Then tuning that as the conversation progresses.

# How can I practice?
Unsurprisingly, the best advice is to practice. Practice writing about engineering topics in plain language. We, as engineers, spend a lot of time writing highly technical comments/documentation and, obviously code.

To be a great at solving real world problems, you need to be able to communicate about those problems. Customers, be it in consulting world, or your software users, will use normal “language” they will not be precise at describing their difficulties 
To be good at solving those problems you need to be a great technical communicator.

There is this great, but repeated _ad nauseam_, quote by Albert Einstein 

> If you cannot explain it *simply, you don't understand it well enough.

It points us in the right direction. If we cannot explain it well to people who do share the highly complex context with us, it is us who have more work to do, not them.

I'll be honest, it's a journey. I still catch myself when I go too deep, when I get too excited and want to share all the detail with someone. They don't need it, at least not at the moment.

If you enjoyed reading this, I post more of my thoughts on [Twitter](https://twitter.com/kubamisiorny)!

[1] the tree of abstraction
